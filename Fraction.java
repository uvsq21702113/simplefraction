
public class Fraction {

	private int X;
	private int Y;
		
	
	public String toString() {
		return "Fraction [X=" + X + "/ Y=" + Y + " = " + X/Y + "]";
	}
	
	public Fraction(int x, int y) {
		super();
		X = x;
		Y = y;
	}
	
	
}
